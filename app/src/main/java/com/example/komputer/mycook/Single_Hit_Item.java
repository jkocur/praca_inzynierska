package com.example.komputer.mycook;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

public class Single_Hit_Item extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {


    TextView food_name, food_time, food_description, food_sec, food_thi, food_thstep, food_secstep, food_firs, food_fi_i, food_t_i, food_ca_i, food_ca, food_in_i, food_in;
    ImageView food_image;
    CollapsingToolbarLayout collapsingToolbarLayout;

    String foodId="";

    DrawerLayout drawer;
    ActionBarDrawerToggle toggle;
    FirebaseDatabase database;
    DatabaseReference foods;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single__hit__item);

        database = FirebaseDatabase.getInstance();
        foods = database.getReference("Food");

        food_description = (TextView) findViewById(R.id.food_desc);
        food_name = (TextView) findViewById(R.id.food_n);
        food_time = (TextView) findViewById(R.id.food_t);
        food_image = (ImageView) findViewById(R.id.im_food);
        food_ca_i = (TextView) findViewById(R.id.food_ca_i);
        food_t_i = (TextView) findViewById(R.id.food_t_i);
        food_ca = (TextView) findViewById(R.id.food_ca);
        food_in = (TextView) findViewById(R.id.food_in);
        food_in_i = (TextView) findViewById(R.id.food_in_i);
        food_fi_i = (TextView) findViewById(R.id.food_fi_i);
        food_firs = (TextView) findViewById(R.id.food_firs);
        food_sec = (TextView) findViewById(R.id.food_sec);
        food_secstep = (TextView) findViewById(R.id.food_secstep);
        food_thi = (TextView) findViewById(R.id.food_thi);
        food_thstep = (TextView) findViewById(R.id.food_thstep);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Menu");
        setSupportActionBar(toolbar);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.bringToFront();
       /* navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {

                    case R.id.nav_home:
                        Intent anIntent = new Intent(getApplicationContext(), Home.class);
                        startActivity(anIntent);
                        drawer.closeDrawers();
                        break;
                    case R.id.nav_search:
                        Intent aIntent = new Intent(getApplicationContext(), Search.class);
                        startActivity(aIntent);
                        drawer.closeDrawers();
                        break;
                    case R.id.nav_map:
                        Intent intent = new Intent(getApplicationContext(), MapsActivity.class);
                        startActivity(intent);
                        drawer.closeDrawers();
                        break;
                    case R.id.nav_video:
                        Intent vintent = new Intent(getApplicationContext(), VideoActivity.class);
                        startActivity(vintent);
                        drawer.closeDrawers();
                        break;

                }
                return false;
            }
        });*/




        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collaps_);
        collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.ExpendedAppBar);
        collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.CollapsedAppBar);

        if (getIntent() != null)
            foodId = getIntent().getStringExtra("FoodId");
        if (!foodId.isEmpty() && foodId != null)
            getSingleFood(foodId);
    }

    private void getSingleFood(String foodId) {
        foods.child(foodId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Food food = dataSnapshot.getValue(Food.class);

                Picasso.with(getBaseContext()).load(food.getImage())
                        .into(food_image);

                collapsingToolbarLayout.setTitle(food.getName());

                food_time.setText(food.getTime());
                food_name.setText(food.getName());
                food_description.setText(food.getDescribe());
                food_ca.setText(food.getCalories());
                food_in.setText(food.getIngredients());
                food_firs.setText(food.getFirst_Step());
                food_secstep.setText(food.getSecond_Step());
                food_thstep.setText(food.getThird_Step());




            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (drawer.isDrawerOpen(GravityCompat.END)) {
            drawer.closeDrawer(GravityCompat.END);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.allbars, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_right_menu) {
            if (drawer.isDrawerOpen(GravityCompat.END)) {
                drawer.closeDrawer(GravityCompat.END);
            } else {
                drawer.openDrawer(GravityCompat.END);
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            SendUserToHome();
        } else if (id == R.id.nav_search) {
            SendUserToSearch();

        } else if (id == R.id.nav_map) {
            SendUserToMapsActivity();

        } else if (id == R.id.nav_video) {
            SendUserToVideo();

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void SendUserToHome() {
        Intent homeIntent = new Intent(Single_Hit_Item.this, Home.class);
        startActivity(homeIntent);
    }

    private void SendUserToMapsActivity() {
        Intent mapsIntent = new Intent(Single_Hit_Item.this, MapsActivity.class);
        startActivity(mapsIntent);
    }

    private void SendUserToSearch() {
        Intent searchIntent = new Intent(Single_Hit_Item.this, Search.class);
        startActivity(searchIntent);

    }
    private void SendUserToVideo() {
        Intent videoIntent = new Intent(Single_Hit_Item.this, VideoActivity.class);
        startActivity(videoIntent);

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        toggle.syncState();
    }
}
