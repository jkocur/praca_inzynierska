package com.example.komputer.mycook;

import android.support.annotation.NonNull;
import android.view.MenuItem;

interface onNavItem {
    @SuppressWarnings("StatementWithEmptyBody")
    boolean onNavigationItemSelected(@NonNull MenuItem item);
}
