package com.example.komputer.mycook;

public class Food {

    private String Name, Image, Describe, Ingredients, First_Step, Second_Step, Third_Step, Searching, MenuId;

    private String Time, Calories;

    public Food() {
    }

    public Food(String name, String image, String describe, String ingredients, String first_Step, String second_Step, String third_Step, String searching, String menuId, String time, String calories) {
        Name = name;
        Image = image;
        Describe = describe;
        Ingredients = ingredients;
        First_Step = first_Step;
        Second_Step = second_Step;
        Third_Step = third_Step;
        Searching = searching;
        MenuId = menuId;
        Time = time;
        Calories = calories;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public String getDescribe() {
        return Describe;
    }

    public void setDescribe(String describe) {
        Describe = describe;
    }

    public String getIngredients() {
        return Ingredients;
    }

    public void setIngredients(String ingredients) {
        Ingredients = ingredients;
    }

    public String getFirst_Step() {
        return First_Step;
    }

    public void setFirst_Step(String first_Step) {
        First_Step = first_Step;
    }

    public String getSecond_Step() {
        return Second_Step;
    }

    public void setSecond_Step(String second_Step) {
        Second_Step = second_Step;
    }

    public String getThird_Step() {
        return Third_Step;
    }

    public void setThird_Step(String third_Step) {
        Third_Step = third_Step;
    }

    public String getSearching() {
        return Searching;
    }

    public void setSearching(String searching) {
        Searching = searching;
    }

    public String getMenuId() {
        return MenuId;
    }

    public void setMenuId(String menuId) {
        MenuId = menuId;
    }

    public String getTime() {
        return Time;
    }

    public void setTime(String time) {
        Time = time;
    }

    public String getCalories() {
        return Calories;
    }

    public void setCalories(String calories) {
        Calories = calories;
    }
}