# Project Name
> MyCook

## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Status](#status)


## General info
A mobile application for browsing recipes created as part of an engineering thesis.
Searching for recipes is to imitate your cooking ingredients, so the search results are only those recipes that meet all search points, not those that meet any of them.
The application shows on Google maps nearby restaurants and has video recipes from the Youtube


## Technologies
* Java
* Firebase Realtime Database 12.0.1
* Algolia 1.0


## Status
Project is:  _finished_


